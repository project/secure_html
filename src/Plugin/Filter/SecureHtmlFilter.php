<?php

namespace Drupal\secure_html\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * A filter that wraps content in an iframe.
 *
 * @Filter(
 *   id = "filter_secure_html",
 *   title = @Translation("Secure HTML filter"),
 *   description = @Translation("Wraps content in an iframe"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class SecureHtmlFilter extends FilterBase {

  /**
   * {@inheritDoc}
   */
  public function process($text, $langcode) {
    $attributes_raw = $this->settings['secure_html_settings'] ? explode("\n", $this->settings['secure_html_settings']) : [];
    $attributes = [];
    foreach ($attributes_raw as $str) {
      $str = str_replace('"', '', $str);
      [$k, $v] = explode("=", $str);
      $attributes[$k] = $v;
    }
    $build = [
      '#theme' => 'secure_html',
      '#content' => $text,
      '#attributes' => $attributes,
    ];
    $rendered = \Drupal::service('renderer')->render($build);
    return new FilterProcessResult($rendered);
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['secure_html_settings'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional iframe attributes'),
      '#default_value' => $this->settings['secure_html_settings'],
      '#description' => $this->t('Additional html attributes applied to iframe, one per line, like "width=400". You might be interested in "sandbox" attribute and related values, see <a href="https://www.w3schools.com/tags/att_iframe_sandbox.asp" target="_blank">https://www.w3schools.com/tags/att_iframe_sandbox.asp</a> for details'),
    ];
    return $form;
  }

}
